import RPi.GPIO as GPIO
import threading
import time


class GpioHandlerThread(threading.Thread):

    def __init__(self, red, yellow, green):
        threading.Thread.__init__(self)
        self.red = red
        self.yellow = yellow
        self.green = green
        self._appState = None
        self.initGpio()

    @property
    def appState(self):
        """ Getter for the state property (private variable) """
        return self._appState

    @appState.setter
    def appState(self, currentState):
        """ Setter for the state property """
        self._appState = currentState

    def initGpio(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.red, GPIO.OUT)
        GPIO.setup(self.yellow, GPIO.OUT)
        GPIO.setup(self.green, GPIO.OUT)
        self.startUpSequence()

    def startUpSequence(self):
        self.ledsOn()
        time.sleep(3)
        self.ledsOff()

    def ledsOn(self):
        GPIO.output(self.red, GPIO.HIGH)
        GPIO.output(self.yellow, GPIO.HIGH)
        GPIO.output(self.green, GPIO.HIGH)

    def ledsOff(self):
        GPIO.output(self.red, GPIO.LOW)
        GPIO.output(self.yellow, GPIO.LOW)
        GPIO.output(self.green, GPIO.LOW)

    def blinkRed(self):
        GPIO.output(self.red, GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(self.red, GPIO.LOW)

    def blinkYellow(self):
        GPIO.output(self.yellow, GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(self.yellow, GPIO.LOW)

    def blinkGreen(self):
        GPIO.output(self.green, GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(self.green, GPIO.LOW)

    def blinkAll(self):
        self.ledsOn()
        time.sleep(0.5)
        self.ledsOff()

    def run(self):
        while True:
            if self._appState == "STOPPED":
                self.blinkRed()
            elif self._appState == "PAUSED":
                self.blinkYellow()
            elif self._appState == "RUNNING":
                self.blinkGreen()
            elif self._appState == "ATTEMPTING":
                self.blinkAll()
            else:
                self.ledsOff()

            time.sleep(5)

import threading
import time
import gps

class GpsPollingThread(threading.Thread):
    def __init__(self, threadID):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self._state = "RUNNING"
        self.session = gps.gps("localhost", "2947")
        self.session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)
        self.location = {'lat': None, 'lng': None}

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, newState):
        self._state = newState

    def getLocation(self):
        return self.location

    def run(self):
        try:
            while True:
                if self._state == "RUNNING":
                    report = self.session.next()
                    if report['class'] == 'TPV':
                        if hasattr(report, 'lat') and hasattr(report, 'lon'):
                            self.location['lat'] = round(report.lat, 6)
                            self.location['lng'] = round(report.lon, 6)
                elif self._state == "STOPPED":
                    return
        except StopIteration:
            pass
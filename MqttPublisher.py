import threading
import datetime
import paho.mqtt.client as paho
import json
import time
from queue import Queue, Empty


class MqttPublisherThread(threading.Thread):
    def __init__(self, threadID, serialQueue, username, password, host, port, baseTopic, topic, createdRideId, gpsPollingThread):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self._state = 'RUNNING'
        self.serialQueue = serialQueue
        self.topic = baseTopic+topic
        self.rideId = createdRideId

        self.MQTT_Message = {'ride_id': createdRideId, 'speed': -1, 'cadence': -1,
                             'power': -1,
                             'latitude': 0, 'longitude': 0,
                             'timestamp': datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')}
        self.gpsPollingThread = gpsPollingThread
        self.mqttClient = self.init_client(host, port, username, password)


    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, newState):
        self._state = newState

    def init_client(self, mqtt_host, mqtt_port, username, password):
        client = paho.Client(transport='websockets')
        client.username_pw_set(username, password)
        client.connect(mqtt_host, mqtt_port)
        client.loop_start()        
        return client

    def createMqttMessage(self):
        powerBuf = [0]
        speedBuf = [0]
        cadenceBuf = [0]
        # while there are values in the thread safe buffer read all of them and append them to arrays
        while not self.serialQueue.empty():
            serial_message = self.serialQueue.get()
            powerBuf.append(serial_message['power'])
            speedBuf.append(serial_message['speed'])
            cadenceBuf.append(serial_message['cadence'])
        
        # perform an average on the serial received values and update the result to the mqtt_message dict
        if len(powerBuf) is not 1:
            try:
                location = self.gpsPollingThread.getLocation()
                self.MQTT_Message['ride_id'] = self.rideId
                self.MQTT_Message['power'] = int(sum(powerBuf) / (len(powerBuf) - 1))
                self.MQTT_Message['speed'] = int((sum(speedBuf) / (len(speedBuf) - 1)))  # must be scaled down with 10 for 1 floating decimal
                self.MQTT_Message['cadence'] = int(sum(cadenceBuf) / (len(cadenceBuf) - 1))
                self.MQTT_Message['latitude'] = location['lat']
                self.MQTT_Message['longitude'] = location['lng']
                self.MQTT_Message['timestamp'] = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
            except ZeroDivisionError as zeroDivision:
                pass

    def run(self):
        while True:
            if self._state == 'RUNNING':
                # Create the final mqtt_message to be sent via MQTT as well as posted to the Server's API endpoint
                self.createMqttMessage()
                if self.MQTT_Message['speed'] >= 0 and None not in self.MQTT_Message:
                    json_mqtt_message = json.dumps(self.MQTT_Message)
                    (rc, mid) = self.mqttClient.publish(self.topic, json_mqtt_message, 0)
            elif self._state == "STOPPED":
                self.mqttClient.disconnect()
                return
            time.sleep(1)

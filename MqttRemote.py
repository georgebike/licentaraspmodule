import datetime
import json
from queue import Queue

import paho.mqtt.client as paho
import threading
import time
import requests

from GpioHandler import GpioHandlerThread
from SerialReader import SerialReadThread
from MqttPublisher import MqttPublisherThread
from GpsPolling import GpsPollingThread


class MqttRemoteThread(threading.Thread):
    """
        This threading class will handle all the communication with the user's phone (the app's remote)
    """

    def __init__(self, threadID, config, host, command_port, statusTopic, commandTopic, username, password):
        threading.Thread.__init__(self)
        self.threadID = threadID

        self.config = config  # config parser

        # Mqtt remote initialization
        self.statusTopic = statusTopic  # The MQTT topic that will route the ride data to the Front End
        self.commandTopic = commandTopic  # The MQTT topic that will route the commands from the Mobile App as well as provide the application state
        self.mqttClient = self.init_client(host, command_port, username, password)

        # Mqtt remote message splitting
        self._appState = None
        self.statusPayload = None
        self.riderTopic = None
        self.riderToken = None

        self.createdRideId = None

        self.serialReadThread = None
        self.mqttPublisherThread = None
        self.serverPosterThread = None
        self.gpsPollingThread = None

        self.gpioHandlerThread = GpioHandlerThread(red=22, yellow=23, green=24)
        self.gpioHandlerThread.start()

    @property
    def appState(self):
        """ Getter for the state property (private variable) """
        return self._appState

    @appState.setter
    def appState(self, currentState):
        """ Setter for the state property """
        self._appState = currentState

    def init_client(self, mqtt_host, mqtt_command_port, username, password):
        """ Init mqtt client to communicate with the mobile app """

        # Looks like the java client can't communicate over websockets so we use TCP transport
        client = paho.Client(transport='tcp')
        client.on_connect = self.on_connect
        client.on_message = self.on_message
        client.username_pw_set(username, password)
        client.connect(mqtt_host, mqtt_command_port)
        client.loop_start()  # Start a non-blocking, polling thread that will handle the MQTT comm. in background
        return client

    def on_connect(self, client, userdata, flags, rc):
        """ MQTT Callback - subscribe to receive commands from the mobile app """
        client.subscribe(self.commandTopic, qos=1)

    def on_message(self, client, userdata, message):
        """ MQTT Callback - called when receiving new message (command) from the mobile app """
        payload_json = json.loads(message.payload.decode())
        print(message.payload.decode())

        # If a new ride is created, additional data will be received from Mobile (user's topic and token). Otherwise, just set the new state
        if payload_json['state'] == "NEW":
            self.riderTopic = payload_json['topic']
            self.riderToken = payload_json['token']
            self._appState = payload_json['state']
        else:
            self._appState = payload_json['state']

    @staticmethod
    def createRide(ride_date, api_token, backend_url_ride):
        """
            Perform a POST request to the backend to create a new ride
        """
        data = {"date": ride_date}
        payload = json.dumps(data)
        headers = {"Content-type": "application/json", "api-token": api_token}
        response = requests.post(backend_url_ride, data=payload, headers=headers)
        if response.status_code == 201:
            return response.json()
        else:
            return None

    def attemptNewRide(self):
        """
            Attempt to create a new ride on backend. On ride successfully created, change state to READY and save ride id
            Ride ID will be used further to save ride data to the specified ride ID
        """
        self._appState = 'BUSY'
        attempts = 5
        ride_date = datetime.datetime.now().strftime('%Y-%m-%d')
        api_url_ride = self.config.get('DEFAULT', 'API_URL_RIDE')
        while attempts > 0:
            createdRide = MqttRemoteThread.createRide(ride_date, self.riderToken, api_url_ride)
            if createdRide is not None:
                self.createdRideId = createdRide['id']
                self._appState = 'READY'
                return
            attempts -= 1
            time.sleep(3)
        self._appState = 'STOP'

    def startApp(self):
        self.gpioHandlerThread.state = 'RUNNING'
        self.sendMqttStatus('RUNNING', 'Recording ride data')
        self._appState = "BUSY"

        """ create queues for passing data between threads in a thread safe manner """
        serialQueue = Queue()  # used for thread-safe passing data coming from serial

        self.serialReadThread = SerialReadThread(1, serialQueue, '/dev/ttyACM0', 115200)
        self.gpsPollingThread = GpsPollingThread(2)
        self.mqttPublisherThread = MqttPublisherThread(3, serialQueue,
                                                       self.config.get('DEFAULT', 'MQTT_USERNAME'),
                                                       self.config.get('DEFAULT', 'MQTT_PASSWORD'),
                                                       self.config.get('DEFAULT', 'MQTT_URL'),
                                                       int(self.config.get('DEFAULT', 'MQTT_DATA_PORT')),
                                                       self.config.get('DEFAULT', 'BASE_TOPIC'),
                                                       self.riderTopic, self.createdRideId, self.gpsPollingThread)

        self.serialReadThread.start()
        self.gpsPollingThread.start()
        self.mqttPublisherThread.start()

    def pauseApp(self):
        self.gpioHandlerThread.state = 'PAUSED'
        self.sendMqttStatus('PAUSED', 'Paused recording ride data')
        self._appState = "BUSY"

        # Set each thread attribute to paused to halt their execution
        self.serialReadThread.state = "PAUSED"
        self.mqttPublisherThread.state = "PAUSED"
        self.gpsPollingThread.state = "PAUSED"

    def resumeApp(self):
        self.gpioHandlerThread.state = 'RUNNING'
        self.sendMqttStatus('RUNNING', 'Recording ride data')
        self._appState = "BUSY"

        # Set each thread attribute to running to resume their execution
        self.serialReadThread.state = "RUNNING"
        self.mqttPublisherThread.state = "RUNNING"
        self.gpsPollingThread.state = "RUNNING"

    def stopApp(self):
        self.gpioHandlerThread.state = 'STOPPED'
        self._appState = "BUSY"
        try:
            self.serialReadThread.state = "STOPPED"
            self.mqttPublisherThread.state = "STOPPED"
            self.gpsPollingThread.state = "STOPPED"

            self.sendMqttStatus('STOPPED', 'Stopped recording ride data and ended current ride')  # Send STOP status
        except AttributeError:
            # If the ride could not be created send a state to the mobile app
            self.sendMqttStatus('FAILURE', 'There was a problem creating a new ride')

    def sendMqttStatus(self, state, message):
        mqtt_message = {'state': state, 'message': message}
        json_mqtt_message = json.dumps(mqtt_message)
        print(json_mqtt_message)
        self.mqttClient.publish(self.statusTopic, json_mqtt_message)

    def run(self):
        while True:
            if self._appState == "NEW":  # remote command
                self.attemptNewRide()
                pass
            elif self._appState == "BUSY":
                # A critical method is executing
                pass
            elif self._appState == "READY":
                self.sendMqttStatus('READY', 'Ride succesfully created. Ready for incoming ride data')
                self._appState = "BUSY"
                pass
            elif self._appState == "START":  # remote command
                self.startApp()
            elif self._appState == "RESUME":  # remote command
                self.resumeApp()
            elif self._appState == "PAUSE":  # remote command
                self.pauseApp()
            elif self._appState == "STOP":  # remote command
                self.stopApp()
            else:
                pass
            time.sleep(1)

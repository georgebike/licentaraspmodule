import threading
import time
import serial


class SerialReadThread(threading.Thread):
    def __init__(self, threadID, serialQueue, serialPort, baudRate):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self._state = 'RUNNING'
        self.serialQueue = serialQueue
        self.serialPort = serialPort
        self.baudRate = baudRate
        self.ser = serial.Serial(self.serialPort, self.baudRate)

        self.incoming_message = {'speed': 0, 'cadence': 0, 'power': 0}

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, newState):
        self._state = newState

    def serReader(self, ser, incoming_message):
        # Read from serial buffer and decode the values from bytes
        # Add the decoded values to the corresponding incoming_message dictionary

        msg = ser.read(6)
        msg_speed = int.from_bytes(msg[0:2], byteorder='little')
        if msg_speed > 65000:
            msg_speed = 0
        incoming_message['speed'] = msg_speed

        msg_cadence = int.from_bytes(msg[2:4], byteorder='little')
        incoming_message['cadence'] = msg_cadence

        msg_power = int.from_bytes(msg[4:], byteorder='little')
        incoming_message['power'] = msg_power

        # Append the mqtt_message dictionary to the thread safe queue
        self.serialQueue.put(incoming_message)

    def run(self):
        while True:
            if self._state == 'RUNNING':
                # if bytes found in input serial buffer, call the read method to read exactly 6
                # bytes (2 bytes for each of the 3 values) and flush the buffer for the next incoming message

                if self.ser.inWaiting() > 0:
                    self.serReader(self.ser, self.incoming_message)
                    self.ser.flushInput()
                    self.ser.reset_input_buffer()

            elif self._state == 'STOPPED':
                self.ser.close()
                return

            time.sleep(0.17)    # cyclic execution with a period of 170 ms

#!/usr/bin/env python
import datetime
import time
from queue import Queue
import configparser
from urllib.request import urlopen
import urllib.error

from MqttRemote import MqttRemoteThread


def main():
    config = configparser.ConfigParser()
    config.optionxform = str
    config.read('appconfig.cfg')

    ''' Get string values from config file '''
    mqtt_remote_status_topic = config.get('DEFAULT', 'STATUS_TOPIC')
    mqtt_remote_command_topic = config.get('DEFAULT', 'COMMAND_TOPIC')
    mqtt_url = config.get('DEFAULT', 'MQTT_URL')
    mqtt_command_port = int(config.get('DEFAULT', 'MQTT_COMMAND_PORT'))
    mqtt_username = config.get('DEFAULT', 'MQTT_USERNAME')
    mqtt_password = config.get('DEFAULT', 'MQTT_PASSWORD')

    # Instantiate a new MqttRemoteThread that will act as an interface between the user and the rest of the app
    mqttRemoteThread = MqttRemoteThread(0, config, mqtt_url, mqtt_command_port, mqtt_remote_status_topic,
                                        mqtt_remote_command_topic,
                                        mqtt_username, mqtt_password)
    mqttRemoteThread.start()


def wait_internet_connection():
    """
        This script will be called from a linux crontab at startup so the app must hang until
        an internet connection is established
    """
    while True:
        try:
            response = urlopen('https://google.com', timeout=3)
            return
        except urllib.error.URLError:
            pass


if __name__ == '__main__':
    wait_internet_connection()
    main()
